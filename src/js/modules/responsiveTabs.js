/*!
 * Tabs
 * Copyright (c) 2015 Hemerson Vianna
 * Licensed under the MIT license
 */
"use strict";
var _tabs = function(element, options) {
    if (!options) { options = {}; }

    this.addEventListeners();
};

_tabs.prototype.addEventListeners = function() {

};

exports.default = _tabs;
